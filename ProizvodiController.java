package nkrivanek.vub.javaprojekt.fxml;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.Node;
import nkrivanek.vub.javaprojekt.klasezapodatke.NeprehrambeniProizvod;
import nkrivanek.vub.javaprojekt.klasezapodatke.PrehrambeniProizvod;
import nkrivanek.vub.javaprojekt.klasezapodatke.Proizvod;
import nkrivanek.vub.javaprojekt.klasezapodatke.Skladiste;
import nkrivanek.vub.javaprojekt.podaci.ProizvodCRUD;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class ProizvodiController {
    @FXML
    private Button btnUredi;
    @FXML
    private Button btnIzbrisi;

    @FXML
    private ListView lstProizvodi;
    private String nazivSkladista;

    private Stage stage;
    private Scene scene;
    private Parent root;


    public void promjenaUScenuSkladista(ActionEvent event) throws IOException {
        root = FXMLLoader.load(getClass().getResource("skladista.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void postaviProizvode(String nazivSkladista) {
        this.nazivSkladista = nazivSkladista; //postavljamo u varijablu kako bismo mogli poslati u sljedeci ekran Proizvod
        ProizvodCRUD proizvodCRUD = new ProizvodCRUD();
        List<Proizvod> proizvodi = proizvodCRUD.dohvatiProizvode(nazivSkladista);
        ObservableList<Proizvod> items = FXCollections.observableArrayList();
        items.addAll(proizvodi);
        lstProizvodi.setItems(items);
        btnUredi.disableProperty().bind(lstProizvodi.getSelectionModel().selectedItemProperty().isNull());
        btnIzbrisi.disableProperty().bind(lstProizvodi.getSelectionModel().selectedItemProperty().isNull());
    }
    public void spremiProizvod(Proizvod proizvod){
        lstProizvodi.getItems().add(proizvod);
    }

    public void dodajProizvod(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("proizvod.fxml"));
        root = loader.load();
        ProizvodController proizvodController = loader.getController();
        proizvodController.postaviProizvod(null, nazivSkladista);
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void urediProizvod(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("proizvod.fxml"));
        root = loader.load();
        Proizvod proizvod = (Proizvod) lstProizvodi.getSelectionModel().selectedItemProperty().getValue();
        ProizvodController proizvodController = loader.getController();
        proizvodController.postaviProizvod(proizvod, nazivSkladista);
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void obrisiProizvod(){
        int index = lstProizvodi.getSelectionModel().getSelectedIndex();
        lstProizvodi.getItems().remove(index);
    }

    public void spremiPromjene(){
        ProizvodCRUD proizvodCRUD = new ProizvodCRUD();
        List<Proizvod> proizvodi = lstProizvodi.getItems();
        proizvodCRUD.spremiProizvode(proizvodi, nazivSkladista);
    }
//Lambda izraz za naprehrambene proizvode
    public void filtrirajNeprehrambene(){
        ProizvodCRUD proizvodCRUD = new ProizvodCRUD();
        List<Proizvod> proizvodi = proizvodCRUD.dohvatiProizvode(nazivSkladista);
        List<Proizvod> neprehrambeniProizvodi=  proizvodi.stream().filter(proizvod -> proizvod instanceof NeprehrambeniProizvod)
                .collect(Collectors.toList());//da li je svaki proizvod instanca neprehrambenog proizvoda ako je dodaj ga u listu
        ObservableList<Proizvod> items = FXCollections.observableArrayList();
        items.addAll(neprehrambeniProizvodi);
        lstProizvodi.setItems(items);
    }

    public void filtrirajPrehrambene(){
        ProizvodCRUD proizvodCRUD = new ProizvodCRUD();
        List<Proizvod> proizvodi = proizvodCRUD.dohvatiProizvode(nazivSkladista);
        List<Proizvod> prehrambeniProizvodi=  proizvodi.stream().filter(proizvod -> proizvod instanceof PrehrambeniProizvod)
                .collect(Collectors.toList());
        ObservableList<Proizvod> items = FXCollections.observableArrayList();
        items.addAll(prehrambeniProizvodi);
        lstProizvodi.setItems(items);
    }

    public void prikaziSve(){
        ProizvodCRUD proizvodCRUD = new ProizvodCRUD();
        List<Proizvod> proizvodi = proizvodCRUD.dohvatiProizvode(nazivSkladista);
        ObservableList<Proizvod> items = FXCollections.observableArrayList();
        items.addAll(proizvodi);
        lstProizvodi.setItems(items);
    }
}
