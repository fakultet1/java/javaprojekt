package nkrivanek.vub.javaprojekt.fxml;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.Node;
import nkrivanek.vub.javaprojekt.klasezapodatke.NeprehrambeniProizvod;
import nkrivanek.vub.javaprojekt.klasezapodatke.PrehrambeniProizvod;
import nkrivanek.vub.javaprojekt.klasezapodatke.Proizvod;
import nkrivanek.vub.javaprojekt.klasezapodatke.Skladiste;
import nkrivanek.vub.javaprojekt.podaci.ProizvodCRUD;
import nkrivanek.vub.javaprojekt.podaci.SkladistaCRUD;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class ProizvodController {
    @FXML
    private TextField txtFnaziv;
    @FXML
    private TextField txtFproizvodac;
    @FXML
    private TextField txtFpdv;
    @FXML
    private TextField txtFsifraProiz;
    @FXML
    private TextField txtFcijena;
    @FXML
    private CheckBox chBoxJamstvo;
    @FXML
    private CheckBox chBoxSmart;
    @FXML
    private TextField txtFalergeni;
    @FXML
    private TextField txtFrokTrajanja;
    @FXML
    private TextField txtFnetoKolicina;
    @FXML
    private Button btnOdustani;
    @FXML
    private Button btnSpremi;
    @FXML
    private ListView lstProizvod;
    @FXML
    private ComboBox cmbBoxVrsta;

    private String nazivSkladista; //koristimo je kako bismo zapamtili u kojem smo skladistu

    private Stage stage;
    private Scene scene;
    private Parent root;

    public void postaviProizvod(Proizvod proizvod, String nazivSkladista) {
        this.nazivSkladista = nazivSkladista;
        if(proizvod != null) {//provjeravam da li je proizvod null jer istu metodu koristimo za uredivanje i dodavanje proizvoda
            txtFnaziv.setText(proizvod.getNaziv());
            txtFproizvodac.setText(proizvod.getNaziv());
            txtFpdv.setText(proizvod.getPDV());
            txtFsifraProiz.setText(String.valueOf(proizvod.getSifraProizvoda()));
            txtFcijena.setText(String.valueOf(proizvod.getCijena()));
            if (proizvod instanceof NeprehrambeniProizvod) {
                cmbBoxVrsta.getSelectionModel().select(1);
                chBoxJamstvo.setSelected(((NeprehrambeniProizvod) proizvod).isJamstvo()); //to je za neprehrambene proizvode,castam iz proizvoda u neprehrambeni
                chBoxSmart.setSelected(((NeprehrambeniProizvod) proizvod).isSmartUredaj());
            }
            if (proizvod instanceof PrehrambeniProizvod) {
                cmbBoxVrsta.getSelectionModel().select(0);
                txtFalergeni.setText(((PrehrambeniProizvod) proizvod).getAlergeni()); //to je za prehrambene proizvode,castam iz proizvoda u prehrambeni
                txtFrokTrajanja.setText(((PrehrambeniProizvod) proizvod).getRokTrajanja());
                txtFnetoKolicina.setText(((PrehrambeniProizvod) proizvod).getNetoKolicina());
            }
        }
    }

    public void spremiProizvod(ActionEvent event) throws IOException {
        Proizvod proizvod = null;
        if("Prehrambeni".equals(cmbBoxVrsta.getSelectionModel().getSelectedItem())){
            PrehrambeniProizvod prehrambeniProizvod = new PrehrambeniProizvod();
            prehrambeniProizvod.setNaziv(txtFnaziv.getText());
            prehrambeniProizvod.setProizvodac(txtFproizvodac.getText());
            prehrambeniProizvod.setCijena(Double.valueOf(txtFcijena.getText()));
            prehrambeniProizvod.setPDV(txtFpdv.getText());
            prehrambeniProizvod.setSifraProizvoda(Long.valueOf(txtFsifraProiz.getText()));
            prehrambeniProizvod.setAlergeni(txtFalergeni.getText());
            prehrambeniProizvod.setRokTrajanja(txtFrokTrajanja.getText());
            prehrambeniProizvod.setNetoKolicina(txtFnetoKolicina.getText());
            proizvod = prehrambeniProizvod;
        }else if("Neprehrambeni".equals(cmbBoxVrsta.getSelectionModel().getSelectedItem())){
            NeprehrambeniProizvod neprehrambeniProizvod = new NeprehrambeniProizvod();
            neprehrambeniProizvod.setNaziv(txtFnaziv.getText());
            neprehrambeniProizvod.setProizvodac(txtFproizvodac.getText());
            neprehrambeniProizvod.setCijena(Double.valueOf(txtFcijena.getText()));
            neprehrambeniProizvod.setPDV(txtFpdv.getText());
            neprehrambeniProizvod.setSifraProizvoda(Long.valueOf(txtFsifraProiz.getText()));
            neprehrambeniProizvod.setJamstvo(chBoxJamstvo.isSelected());
            neprehrambeniProizvod.setSmartUredaj(chBoxSmart.isSelected());;
            proizvod=neprehrambeniProizvod;
        } else {
            //TODO dodati da baci gresku
        }

        FXMLLoader loader = new FXMLLoader(getClass().getResource("proizvodi.fxml"));
        root = loader.load();
        ProizvodiController proizvodiController = loader.getController();
        proizvodiController.postaviProizvode(nazivSkladista);
        proizvodiController.spremiProizvod(proizvod);
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    public void promjenaUScenuProizvodi(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("proizvodi.fxml"));
        root = loader.load();
        ProizvodiController proizvodiController = loader.getController();
        proizvodiController.postaviProizvode(nazivSkladista);
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

}
