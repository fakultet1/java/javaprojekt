package nkrivanek.vub.javaprojekt.fxml;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import nkrivanek.vub.javaprojekt.podaci.SkladistaCRUD;

import java.io.IOException;

public class LoginController {

    private String greska = "Pogrešno korisničko ime ili zaporka. Molimo pokušajte ponovno";
    private String korisnickoIme = "ggrubisic";
    private String korisnickaLozinka = "123";
    private String imePrezime = "Gordana Grubišić";
    @FXML
    private TextField txtFkorIme;
    @FXML
    private TextField txtFlozinka;
    @FXML
    private Button btnLogin;
    @FXML
    private Text txtGreska;

    private Stage stage;
    private Scene scene;
    private Parent root;

    @FXML
    private void onLogin(ActionEvent event) {
        String korIme = txtFkorIme.getText();
        String lozinka = txtFlozinka.getText();

        if (korIme.equals(korisnickoIme) && lozinka.equals(korisnickaLozinka)) {
            try {
                promjenaUScenuSkladista(event);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            txtGreska.setText(greska);
        }
    }

    public void promjenaUScenuSkladista(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("skladista.fxml"));
        root = loader.load();
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}
