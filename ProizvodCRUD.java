package nkrivanek.vub.javaprojekt.podaci;


import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import com.fasterxml.jackson.databind.jsontype.PolymorphicTypeValidator;
import nkrivanek.vub.javaprojekt.klasezapodatke.Proizvod;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;


public class ProizvodCRUD {

    private String putanja = "src/main/resources/nkrivanek/vub/javaprojekt/podaci/";
    static Logger log = Logger.getLogger("nkrivanek.vub.javaprojekt.podaci.ProizvodCRUD");

    public List<Proizvod> dohvatiProizvode(String nazivSkladista) {
        List<Proizvod> proizvodi = new ArrayList<>();
        try {
            // create object mapper instance
            ObjectMapper mapper = new ObjectMapper();
            File datoteka = Paths.get(putanja + nazivSkladista + ".json").toFile();

            log.info("Provjera postoji li datoteka za skladiste: " + nazivSkladista);
            if (!datoteka.exists()) {
                log.info("Ne postoji datoteka za skladiste: " + nazivSkladista);
                datoteka.createNewFile();
                log.info("Datoteka za skladiste: " + nazivSkladista + " uspjesno kreirana");
            }
            if (datoteka.length() > 0) {//provjeravam da li je datoteka prazna
                log.info("Dohvacam podatke za skladiste: " + nazivSkladista);
                proizvodi = Arrays.asList(mapper.readValue(datoteka, Proizvod[].class));
            }
        } catch (IOException ex) {
            log.severe("Greska kod dohvata podataka za skladiste: " + nazivSkladista + ", opis greske: " + ex.getMessage());
        }
        return proizvodi;
    }

    public void spremiProizvode(List<Proizvod> proizvodi, String nazivSkladista) {
        try {
            log.info("Spremam podatke za skladiste: " + nazivSkladista);
            // create object mapper instance
            ObjectWriter objectWriter = new ObjectMapper().writerFor(new TypeReference<List<Proizvod>>() {});//koristim objectWriter da se zapise i JsonTypeInfo u json
            objectWriter.writeValue(Paths.get(putanja + nazivSkladista + ".json").toFile(), proizvodi);
        } catch (IOException ex) {
            log.severe("Greska kod spremanja podataka za skladiste: " + nazivSkladista + ", opis greske: " + ex.getMessage());
        }

    }
}
