package nkrivanek.vub.javaprojekt.fxml;

import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.Node;
import nkrivanek.vub.javaprojekt.klasezapodatke.Skladiste;
import nkrivanek.vub.javaprojekt.podaci.SkladistaCRUD;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class SkladistaController implements Initializable {

    @FXML
    private Text txtNazivSkladiste;
    @FXML
    private Text txtOIBSladiste;
    @FXML
    private Text txtAdresaSkladiste;
    @FXML
    private Text txtKontaktSkladiste;
    @FXML
    private Button btnProizvodi;
    @FXML
    private ListView lstSkladista;

    private Stage stage;
    private Scene scene;
    private Parent root;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SkladistaCRUD skladistaCRUD = new SkladistaCRUD();
        List<Skladiste> skladista = skladistaCRUD.dohvatSkladista();
        ObservableList<Skladiste> items = FXCollections.observableArrayList();
        items.addAll(skladista);
        lstSkladista.setItems(items);
        postaviOnChangeListener();
        btnProizvodi.disableProperty()
                .bind(lstSkladista.getSelectionModel().selectedItemProperty().isNull());
    }

    public void promjenaUScenuProizvodi(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("proizvodi.fxml"));
        root = loader.load();

        Skladiste skladiste = (Skladiste) lstSkladista.getSelectionModel().selectedItemProperty().getValue();
        ProizvodiController proizvodiController = loader.getController();
        proizvodiController.postaviProizvode(skladiste.getNaziv());

        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    //metoda za postavljanje Listenera koji osluskuje kada ce se dogoditi klik. Kada se dogodi klik kazem mu da prikaze podatke
    private void postaviOnChangeListener() {
        lstSkladista.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Skladiste>() {
            @Override
            public void changed(ObservableValue observableValue, Skladiste staroSkladiste, Skladiste novoSkladiste) {
                txtNazivSkladiste.setText(novoSkladiste.getNaziv());
                txtOIBSladiste.setText(novoSkladiste.getOIB());
                txtAdresaSkladiste.setText(novoSkladiste.getAdresa());
                txtKontaktSkladiste.setText(novoSkladiste.getKontakt());
            }
        });
    }


}
