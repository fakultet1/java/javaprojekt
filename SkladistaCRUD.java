package nkrivanek.vub.javaprojekt.podaci;

import nkrivanek.vub.javaprojekt.klasezapodatke.Skladiste;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

public class SkladistaCRUD {

    private String putanja = "src/main/resources/nkrivanek/vub/javaprojekt/podaci/skladista.json";
    static Logger log = Logger.getLogger("nkrivanek.vub.javaprojekt.podaci.SkladistaCRUD");


    public List<Skladiste> dohvatSkladista() {
        List<Skladiste> skladista = new ArrayList<>();
        try {
            log.info("Provjera postoji li datoteka za skladista");
            File datoteka = Paths.get(putanja).toFile();
            if (!datoteka.exists()) {
                log.info("Ne postoji datoteka za skladista");
                datoteka.createNewFile();
                log.info("Datoteka za skladista uspjesno kreirana");
            }
            if (datoteka.length() > 0) {
                // create object mapper instance
                ObjectMapper mapper = new ObjectMapper();
                log.info("Dohvacam skladista");
                skladista = Arrays.asList(mapper.readValue(datoteka, Skladiste[].class));
            }
        } catch (IOException ex) {
            log.severe("Greska kod dohvata podataka za skladista, opis greske: " + ex.getMessage());
        }

        return skladista;
    }
}
