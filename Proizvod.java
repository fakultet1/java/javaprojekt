package nkrivanek.vub.javaprojekt.klasezapodatke;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

//dodatni atribut u JSON-u pomocu kojeg jackson mapper sazna koju klasu treba
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "vrsta")
@JsonSubTypes({
        @JsonSubTypes.Type(value = PrehrambeniProizvod.class, name = "prehrambeni"),
        @JsonSubTypes.Type(value = NeprehrambeniProizvod.class, name = "neprehrambeni")
})
public abstract class Proizvod {
    private double cijena;
    private String naziv;
    private String proizvodac;
    private String PDV;
    private long sifraProizvoda;

    public Proizvod() {
    }

    //konstruktor
    public Proizvod(double cijena, String naziv, String proizvodac, String PDV, long sifraProizvoda) {
        this.cijena = cijena;
        this.naziv = naziv;
        this.proizvodac = proizvodac;
        this.sifraProizvoda = sifraProizvoda;
    }

    public Proizvod(double cijena, String naziv, String proizvodac) {
        this.cijena = cijena;
        this.naziv = naziv;
        this.proizvodac = proizvodac;
    }

    public Proizvod(String naziv, double cijena) {
        this.naziv = naziv;
        this.cijena = cijena;
    }

    //geteri i setteri

    public double getCijena() {
        return cijena;
    }

    public void setCijena(double cijena) {
        this.cijena = cijena;
    }

    public long getSifraProizvoda() {
        return sifraProizvoda;
    }

    public String getNaziv() {
        return naziv;
    }

    public String getProizvodac() {
        return proizvodac;
    }

    public String getPDV() {
        return PDV;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public void setPDV(String PDV) {
        this.PDV = PDV;
    }

    public void setProizvodac(String proizvodac) {
        this.proizvodac = proizvodac;
    }

    public void setSifraProizvoda(long sifraProizvoda) {
        this.sifraProizvoda = sifraProizvoda;
    }

}
