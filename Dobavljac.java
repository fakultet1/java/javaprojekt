package nkrivanek.vub.javaprojekt.klasezapodatke;

public class Dobavljac extends Proizvod implements Ispisi {
    private String naziv;
    private String ulica;
    private int kucBr;
    private String grad;
    private String drzava;
    private long OIB;
    private String kontakt;

    //konstruktori

    public Dobavljac(String naziv, String ulica, int kucBr, String grad, String drzava, long OIB, String kontakt) {
        this.naziv = naziv;
        this.ulica = ulica;
        this.kucBr = kucBr;
        this.grad = grad;
        this.drzava = drzava;
        this.OIB = OIB;
        this.kontakt = kontakt;
    }

    //setteri i getteri


    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public void setOIB(long OIB) {
        this.OIB = OIB;
    }

    public void setKucBr(int kucBr) {
        this.kucBr = kucBr;
    }

    public void setKontakt(String kontakt) {
        this.kontakt = kontakt;
    }

    public void setDrzava(String drzava) {
        this.drzava = drzava;
    }

    public String getUlica() {
        return ulica;
    }

    public String getKontakt() {
        return kontakt;
    }

    public String getDrzava() {
        return drzava;
    }

    public long getOIB() {
        return OIB;
    }

    public int getKucBr() {
        return kucBr;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getGrad() {
        return grad;
    }

    public void setGrad(String grad) {
        this.grad = grad;
    }

    //implementiranje nkrivanek.vub.javaprojekt.podaci.Ispisi
    @Override
    public void Ispisi() {
        System.out.println("Naziv:" + getNaziv() + "Ulica:" + getUlica() + "Kucni broj:" + getKucBr() + "Grad:" + getGrad() + "OIB:" + getOIB() + "Kontak:" + getKontakt());
    }

}
