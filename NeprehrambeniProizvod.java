package nkrivanek.vub.javaprojekt.klasezapodatke;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("neprehrambeni")
public class NeprehrambeniProizvod extends Proizvod implements Ispisi, CijenaSPopustom {
    private boolean jamstvo;
    private boolean smartUredaj;

    //konstruktori
    public NeprehrambeniProizvod() {
        super();
    }

    public NeprehrambeniProizvod(boolean jamstvo, boolean smartUredaj) {
        this.jamstvo = jamstvo;
        this.smartUredaj = smartUredaj;

    }

    public NeprehrambeniProizvod(String naziv, double cijena, String proizvodac, long sifraProizvoda) {
        super(cijena, naziv, proizvodac);
        setSifraProizvoda(sifraProizvoda);

    }

    public NeprehrambeniProizvod(String naziv, String proizvodac, int cijena) {
        super(cijena, naziv, proizvodac);
    }


    //seteri i getteri
    public boolean isSmartUredaj() {
        return smartUredaj;
    }

    public boolean isJamstvo() {
        return jamstvo;
    }

    public void setJamstvo(boolean jamstvo) {
        this.jamstvo = jamstvo;
    }

    public void setSmartUredaj(boolean smartUredaj) {
        this.smartUredaj = smartUredaj;
    }

    //implementiranje interfacea
    @Override
    public void Ispisi() {
        System.out.println("Naziv:" + getNaziv() + "Smart uredaj:" + isSmartUredaj());
    }

    @Override
    public void CijenaSPopustom(double popust, double cijena) {
        double cijenaPopusta = (popust / 100) * cijena;
        double cijenaSPopustom = cijena - cijenaPopusta;
        System.out.println("Cijena s popustom neprehrambenog proizvoda je:" + cijenaSPopustom);
    }

    @Override
    public String toString() {
        return this.getNaziv() + ", " + this.getCijena() + ", " + this.jamstvo + ", " + this.smartUredaj;
    }

}
