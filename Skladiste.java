package nkrivanek.vub.javaprojekt.klasezapodatke;

public class Skladiste implements Ispisi {
    private String naziv;
    private String ulica;
    private int kucBr;
    private String grad;
    private String drzava;
    private String OIB;
    private String kontakt;
    private String listaProizvoda[];

    //konstruktori
    public Skladiste() {
    }

    public Skladiste(String naziv, String ulica, int kucBr, String grad, String drzava, String OIB, String kontakt) {
        this.naziv = naziv;
        this.ulica = ulica;
        this.grad = grad;
        this.drzava = drzava;
        this.OIB = OIB;
        this.kontakt = kontakt;
    }

    public Skladiste(String naziv, String ulica, int kucBr, String grad, String drzava, String OIB, String kontakt, String listaProizvoda[]) {
        this.naziv = naziv;
        this.ulica = ulica;
        this.grad = grad;
        this.drzava = drzava;
        this.OIB = OIB;
        this.kontakt = kontakt;
        this.listaProizvoda = listaProizvoda;
    }

    //setteri i geteeri

    public void setListaProizvoda(String[] listaProizvoda) {
        this.listaProizvoda = listaProizvoda;
    }

    public void setGrad(String grad) {
        this.grad = grad;
    }

    public String[] getListaProizvoda() {
        return listaProizvoda;
    }

    public String getGrad() {
        return grad;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getNaziv() {
        return naziv;
    }

    public int getKucBr() {
        return kucBr;
    }

    public String getOIB() {
        return OIB;
    }

    public String getDrzava() {
        return drzava;
    }

    public String getKontakt() {
        return kontakt;
    }

    public String getUlica() {
        return ulica;
    }

    public void setDrzava(String drzava) {
        this.drzava = drzava;
    }

    public void setKontakt(String kontakt) {
        this.kontakt = kontakt;
    }

    public void setKucBr(int kucBr) {
        this.kucBr = kucBr;
    }

    public void setOIB(String OIB) {
        this.OIB = OIB;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public String getAdresa() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.ulica);
        sb.append(" ");
        sb.append(this.kucBr);
        sb.append(", ");
        sb.append(this.grad);
        sb.append(", ");
        sb.append(this.drzava);
        return sb.toString();
    }

    public void Ispisi() {
        System.out.println("Naziv:" + getNaziv() + "Ulica:" + getUlica() + "Kucni broj:" + getKucBr() + "Grad:" + getGrad() + "OIB:" + getOIB() + "Kontak:" + getKontakt());
    }

    @Override
    public String toString() {
        return this.naziv + ", " + this.ulica + ", " + this.kucBr + ", " + this.grad;
    }
}
