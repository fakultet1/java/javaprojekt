package nkrivanek.vub.javaprojekt.klasezapodatke;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("prehrambeni")
public class PrehrambeniProizvod extends Proizvod implements Ispisi, CijenaSPopustom {
    private String alergeni;
    private String rokTrajanja;
    private String netoKolicina; //gramaza npr.300 grama ili 900 grama, 1.1 kilogram i slicno

    public PrehrambeniProizvod() {
        super();
    }

    public PrehrambeniProizvod(String naziv, int cijena, String alergeni, String rokTrajanja, String netoKolicina) {
        super(naziv, cijena);
        this.alergeni = alergeni;
        this.rokTrajanja = rokTrajanja;
        this.netoKolicina = netoKolicina;

    }

    public PrehrambeniProizvod(String naziv, int cijena, String alergeni, String proizvodac, String rokTrajanja, long sifraProizvoda) {
        super(cijena, naziv, proizvodac);
        this.alergeni = alergeni;
        this.rokTrajanja = rokTrajanja;
        setSifraProizvoda(sifraProizvoda);
    }

    public String getAlergeni() {
        return alergeni;
    }

    public String getRokTrajanja() {
        return rokTrajanja;
    }

    public String getNetoKolicina() {
        return netoKolicina;
    }

    public void setAlergeni(String alergeni) {
        this.alergeni = alergeni;
    }

    public void setRokTrajanja(String rokTrajanja) {
        this.rokTrajanja = rokTrajanja;
    }

    public void setNetoKolicina(String netoKolicina) {
        this.netoKolicina = netoKolicina;
    }

    //implementacija nkrivanek.vub.javaprojekt.podaci.Ispisi

    @Override
    public void Ispisi() {
        System.out.println("Naziv:" + getNaziv() + "Cijena:" + getCijena() + "Alergeni:" + getAlergeni() + "Rok trajanja:" + getRokTrajanja() + "Neto kolicina:" + getNetoKolicina());
    }

    public void CijenaSPopustom(double popust, double cijena) {
        double cijenaPopusta = (popust / 100) * cijena;
        double cijenaSPopustom = cijena - cijenaPopusta;
        System.out.println("Cijena s popustom prehrambenog proizvoda je:" + cijenaSPopustom);
    }

    @Override
    public String toString() {
        return this.getNaziv() + ", " + this.getCijena() + ", " + this.alergeni + ", " + this.rokTrajanja + ", " + this.netoKolicina;
    }

}
